package org.codefetti.junit.report.reader.util;

import org.junit.Test;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import static org.assertj.core.api.Assertions.assertThat;

public class XmlAttributeUtilTest {


    @Test
    public void shouldReadStringAttribute() throws IOException, XMLStreamException {
        StartElement test = getFirstElement("<string foo=\"bar\"/>", "string");
        String actual = XmlAttributeUtil.getAttribute(test, "foo", null);
        assertThat(actual).isEqualTo("bar");
    }

    @Test
    public void shouldReadDefaultStringIfAttributeIsEmpty() throws IOException,
                                                                    XMLStreamException {
        StartElement test = getFirstElement("<string foo=\"\"/>", "string");
        String actual = XmlAttributeUtil.getAttribute(test, "foo", "my-default");
        assertThat(actual).isEqualTo("my-default");
    }

    @Test
    public void shouldReadDefaultStringIfAttributeIsMissing() throws IOException,
                                                                    XMLStreamException {
        StartElement test = getFirstElement("<string foo=\"bar\"/>", "string");
        String actual = XmlAttributeUtil.getAttribute(test, "bar", "my-default");
        assertThat(actual).isEqualTo("my-default");
    }

    @Test
    public void shouldReadLongAttribute() throws IOException, XMLStreamException {
        StartElement test = getFirstElement("<long foo=\"42\"/>", "long");
        long actual = XmlAttributeUtil.getAttribute(test, "foo", 0);
        assertThat(actual).isEqualTo(42);
    }

    @Test
    public void shouldReadDefaultLongIfAttributeIsEmpty() throws IOException,
                                                                    XMLStreamException {
        StartElement test = getFirstElement("<long foo=\"\"/>", "long");
        long actual = XmlAttributeUtil.getAttribute(test, "foo", 12);
        assertThat(actual).isEqualTo(12);
    }

    @Test
    public void shouldReadDefaultLongIfAttributeIsNotLong() throws IOException,
                                                                    XMLStreamException {
        StartElement test = getFirstElement("<long foo=\"bar\"/>", "long");
        long actual = XmlAttributeUtil.getAttribute(test, "foo", 12);
        assertThat(actual).isEqualTo(12);
    }

    @Test
    public void shouldReadDefaultLongIfAttributeIsMissing() throws IOException,
                                                                    XMLStreamException {
        StartElement test = getFirstElement("<long foo=\"bar\"/>", "long");
        long actual = XmlAttributeUtil.getAttribute(test, "bar", 12);
        assertThat(actual).isEqualTo(12);
    }

    @Test
    public void shouldReadDoubleAttribute() throws IOException, XMLStreamException {
        StartElement test = getFirstElement("<double foo=\"4.2\"/>", "double");
        double actual = XmlAttributeUtil.getAttribute(test, "foo", 0d);
        assertThat(actual).isEqualTo(4.2d);
    }

    @Test
    public void shouldReadDefaultDoubleIfAttributeIsEmpty() throws IOException,
                                                                    XMLStreamException {
        StartElement test = getFirstElement("<double foo=\"\"/>", "double");
        double actual = XmlAttributeUtil.getAttribute(test, "foo", 12d);
        assertThat(actual).isEqualTo(12);
    }

    @Test
    public void shouldReadDefaultDoubleIfAttributeIsNotLong() throws IOException,
                                                                    XMLStreamException {
        StartElement test = getFirstElement("<double foo=\"bar\"/>", "double");
        double actual = XmlAttributeUtil.getAttribute(test, "foo", 12d);
        assertThat(actual).isEqualTo(12);
    }

    @Test
    public void shouldReadDefaultDoubleIfAttributeIsMissing() throws IOException,
                                                                    XMLStreamException {
        StartElement test = getFirstElement("<double foo=\"bar\"/>", "double");
        double actual = XmlAttributeUtil.getAttribute(test, "bar", 12d);
        assertThat(actual).isEqualTo(12);
    }


    private StartElement getFirstElement(String xmlSnippet, String elementName)
            throws XMLStreamException, IOException {
        String xmlStart = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
        try (Reader r = new StringReader(xmlStart + xmlSnippet)) {
            XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(r);
            while (xmlEventReader.hasNext()) {
                XMLEvent xmlEvent = xmlEventReader.nextEvent();
                if (xmlEvent.isStartElement()
                        && elementName.equals(xmlEvent.asStartElement().getName().getLocalPart())) {
                    return xmlEvent.asStartElement();
                }
            }
        }
        return null;
    }
}
