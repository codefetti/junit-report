package org.codefetti.junit.report.reader;

import org.codefetti.junit.report.model.TestCaseReport;
import org.codefetti.junit.report.model.TestSuiteReport;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public class TestSuiteXmlReaderTest {

    private TestSuiteXmlReader reader = new TestSuiteXmlReader();

    @Test
    public void shouldReadTestSuiteReport() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport).isNotNull();
    }

    @Test
    public void shouldReadName() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getName())
                .isEqualTo("org.codefetti.proxy.handler.example.ExampleIT");
    }

    @Test
    public void shouldReadTests() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getTests()).isEqualTo(4);
    }

    @Test
    public void shouldReadSkipped() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getSkipped()).isEqualTo(1);
    }

    @Test
    public void shouldReadErrors() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getErrors()).isEqualTo(0);
    }

    @Test
    public void shouldReadFailures() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getFailures()).isEqualTo(2);
    }

    @Test
    public void shouldReadTimeOfExecution() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        ZonedDateTime expected = ZonedDateTime.parse("2020-01-12T09:10:12+01:00[Europe/Berlin]");
        assertThat(testSuiteReport.getTimeOfExecution())
                .isEqualTo(expected.toInstant());
    }

    @Test
    public void shouldReadDurationOfExecution() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getDurationOfExecution())
                .isEqualByComparingTo(Duration.ofMillis(33));
    }

    @Test
    public void shouldReadTestCasesClassNames() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getTestCases())
                .extracting(TestCaseReport::getClassName)
                .containsExactly(
                        "org.codefetti.proxy.handler.example.ExampleIT",
                        "org.codefetti.proxy.handler.example.ExampleIT",
                        "org.codefetti.proxy.handler.example.ExampleIT",
                        "org.codefetti.proxy.handler.example.ExampleIT"
                );
    }

    @Test
    public void shouldReadTestCasesNames() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getTestCases())
                .extracting(TestCaseReport::getName)
                .containsExactly(
                        "shouldHandleToStringMethod",
                        "shouldHandleInvocedMethod",
                        "shouldBeAbleToCallDefaultMethod",
                        "shouldHandleEqualsMethod"
                );
    }

    @Test
    public void shouldReadTestCasesResults() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getTestCases())
                .extracting(TestCaseReport::getResult)
                .containsExactly(
                        TestCaseReport.Result.PASSED,
                        TestCaseReport.Result.FAILED,
                        TestCaseReport.Result.FAILED,
                        TestCaseReport.Result.SKIPPED
                );
    }

    @Test
    public void shouldReadTestCasesDurationOfExecutions() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getTestCases())
                .extracting(TestCaseReport::getDurationOfExecution)
                .containsExactly(
                        Duration.ofMillis(11),
                        Duration.ofMillis(4),
                        Duration.ofMillis(12),
                        Duration.ofMillis(0)
                );
    }

    @Test
    public void shouldReadTestCasesFailureType() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getTestCases())
                .extracting(TestCaseReport::getFailureType)
                .containsExactly(
                        null,
                        "java.lang.RuntimeException",
                        "org.junit.ComparisonFailure",
                        null
                );
    }

    @Test
    public void shouldReadTestCasesFailureMessage() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getTestCases())
                .extracting(TestCaseReport::getFailureMessage)
                .containsExactly(
                        null,
                        "java.lang.RuntimeException: Foo",
                        "org.junit.ComparisonFailure: expected:<Hello Worl[]> but was:<Hello " +
                                "Worl[d]>",
                        null
                );
    }

    @Test
    public void shouldReadTestCasesFailureLog() throws IOException {
        TestSuiteReport testSuiteReport = readTestSuiteReport(
                "/test-data/TEST-org.codefetti.proxy.handler.example.ExampleIT.xml");

        assertThat(testSuiteReport.getTestCases().get(0).getFailureLog())
                .isNull();
        assertThat(testSuiteReport.getTestCases().get(1).getFailureLog())
                .isEqualTo(readStackTrace("/test-data/failureInvokedMethod.txt"));
        assertThat(testSuiteReport.getTestCases().get(2).getFailureLog())
                .isEqualTo(readStackTrace("/test-data/failureBeAbleToCallDefaultMethod.txt"));
        assertThat(testSuiteReport.getTestCases().get(3).getFailureLog())
                .isNull();
    }

    @Test
    public void shouldUseZeroDurationIfNotSet() throws IOException {
        String source = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<testsuite>\n" +
                "</testsuite>";
        try (InputStream is = new ByteArrayInputStream(source.getBytes(StandardCharsets.UTF_8))) {
            TestSuiteReport testSuiteReport = reader.readTestSuite(is);
            assertThat(testSuiteReport.getDurationOfExecution())
                    .isEqualByComparingTo(Duration.ofMillis(0));
        }
    }

    @Test
    public void shouldUseZeroDurationIfNotParseable() throws IOException {
        String source = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<testsuite time=\"1000ms\">\n" +
                "</testsuite>";
        try (InputStream is = new ByteArrayInputStream(source.getBytes(StandardCharsets.UTF_8))) {
            TestSuiteReport testSuiteReport = reader.readTestSuite(is);
            assertThat(testSuiteReport.getDurationOfExecution())
                    .isEqualByComparingTo(Duration.ofMillis(0));
        }
    }

    @Test
    public void shouldUseNowIfTimeNotSet() throws IOException {
        String source = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<testsuite>\n" +
                "</testsuite>";
        try (InputStream is = new ByteArrayInputStream(source.getBytes(StandardCharsets.UTF_8))) {
            TestSuiteReport testSuiteReport = reader.readTestSuite(is);
            assertThat(testSuiteReport.getTimeOfExecution())
                    .isBetween(Instant.now().minusMillis(100), Instant.now().plusMillis(10));
        }
    }

    @Test
    public void shouldUseNowIfNotParseable() throws IOException {
        String source = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<testsuite timestamp=\"yesterday\">\n" +
                "</testsuite>";
        try (InputStream is = new ByteArrayInputStream(source.getBytes(StandardCharsets.UTF_8))) {
            TestSuiteReport testSuiteReport = reader.readTestSuite(is);
            assertThat(testSuiteReport.getTimeOfExecution())
                    .isBetween(Instant.now().minusMillis(100), Instant.now().plusMillis(10));
        }
    }

    @Test
    public void shouldReturnNullForInvalidXml() throws IOException {
        String source = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<testsuite </testsuite>";
        try (InputStream is = new ByteArrayInputStream(source.getBytes(StandardCharsets.UTF_8))) {
            TestSuiteReport testSuiteReport = reader.readTestSuite(is);
            assertThat(testSuiteReport).isNull();
        }
    }

    private TestSuiteReport readTestSuiteReport(
            @SuppressWarnings("SameParameterValue") String classPathResource) throws IOException {
        try (InputStream xmlReport = getClass().getResourceAsStream(classPathResource)) {
            return reader.readTestSuite(xmlReport);
        }
    }

    private String readStackTrace(String classPathResource) {
        try {
            return new String(
                    Files.readAllBytes(
                            Paths.get(getClass().getResource(classPathResource).toURI())
                    ),
                    StandardCharsets.UTF_8
            );
        } catch (URISyntaxException | IOException ignored) {
            return "READ FAILURE";
        }
    }
}
