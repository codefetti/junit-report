package org.codefetti.junit.report.reader.util;

import javax.xml.namespace.QName;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;

public class XmlAttributeUtil {

    private XmlAttributeUtil() {
        // just a utility
    }

    public static String getAttribute(
            StartElement element,
            String attributeLocalName,
            String defaultValue) {
        Attribute attribute = getAttribute(element, attributeLocalName);
        if (attribute == null) {
            return defaultValue;
        }
        String value = attribute.getValue();
        return value!= null && value.trim().length() > 0 ? value : defaultValue;
    }

    public static long getAttribute(
            StartElement element,
            String attributeLocalName,
            long defaultValue) {
        String value = getAttribute(element, attributeLocalName, null);
        try {
            return Long.parseLong(value);
        }
        catch (NumberFormatException ignored) {
            return defaultValue;
        }
    }

    public static double getAttribute(
            StartElement element,
            String attributeLocalName,
            double defaultValue) {
        String value = getAttribute(element, attributeLocalName, null);
        try {
            return Double.parseDouble(value);
        }
        catch (NullPointerException | NumberFormatException ignored) {
            return defaultValue;
        }
    }

    private static Attribute getAttribute(StartElement element, String attributeLocalName) {
        String namespaceURI = element.getName().getNamespaceURI();
        QName attributeName = new QName(namespaceURI, attributeLocalName);
        return element.getAttributeByName(attributeName);
    }

}
