package org.codefetti.junit.report.reader;

import org.codefetti.junit.report.model.TestCaseReport;
import org.codefetti.junit.report.model.TestSuiteReport;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;
import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;

import static org.codefetti.junit.report.reader.util.XmlAttributeUtil.getAttribute;

/**
 * A reader for JUnit XML files.
 */
public class TestSuiteXmlReader {

    private static final String TESTSUITE_LOCAL_NAME = "testsuite";
    private static final String TESTCASE_LOCAL_NAME = "testcase";

    /**
     * @param xmlContent the content of a JUnit test report XML file
     * @return The {@link TestSuiteReport} described in the {@code xmlContent} or {@code null} if
     * the file can't be read.
     */
    public TestSuiteReport readTestSuite(InputStream xmlContent) {
        try {
            XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
            XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(xmlContent);
            TestSuiteReport testSuiteReport = null;
            while (xmlEventReader.hasNext()) {
                if (isStartElement(xmlEventReader.peek(), TESTSUITE_LOCAL_NAME)) {
                    testSuiteReport = readTestSuiteReport(xmlEventReader);
                }
                else {
                    xmlEventReader.nextEvent();
                }
            }
            return testSuiteReport;
        }
        catch (XMLStreamException e) {
            return null;
        }
    }

    private TestSuiteReport readTestSuiteReport(XMLEventReader xmlEventReader)
            throws XMLStreamException {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        StartElement testSuiteElement = xmlEvent.asStartElement();
        TestSuiteReport testSuiteReport = new TestSuiteReport(
                getAttribute(testSuiteElement, "name", "unknown suite"),
                getAttribute(testSuiteElement, "tests", 0),
                getAttribute(testSuiteElement, "skipped", 0),
                getAttribute(testSuiteElement, "failures", 0),
                getAttribute(testSuiteElement, "errors", 0),
                getTimestamp(testSuiteElement, Instant.now()),
                getTime(testSuiteElement, Duration.ofMillis(0))
        );
        while (xmlEventReader.hasNext()
                && !isEndElement(xmlEventReader.peek(), TESTSUITE_LOCAL_NAME)) {
            if (isStartElement(xmlEventReader.peek(), TESTCASE_LOCAL_NAME)) {
                testSuiteReport.addTestCase(readTestCaseReport(xmlEventReader));
            }
            else {
                xmlEventReader.nextEvent();
            }
        }
        return testSuiteReport;
    }

    private TestCaseReport readTestCaseReport(XMLEventReader xmlEventReader)
            throws XMLStreamException {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        StartElement testCaseElement = xmlEvent.asStartElement();
        TestCaseReport testCaseReport = new TestCaseReport(
                getAttribute(testCaseElement, "classname", "unknown class"),
                getAttribute(testCaseElement, "name", "unknown name"),
                getTime(testCaseElement, Duration.ofMillis(0))
        );
        while (xmlEventReader.hasNext()
                && !isEndElement(xmlEventReader.peek(), TESTCASE_LOCAL_NAME)) {
            if (isStartElement(xmlEventReader.peek(), "skipped")) {
                testCaseReport.classifyAsSkipped();
                xmlEventReader.nextEvent();
            }
            else if (isStartElement(xmlEventReader.peek(), "failure")) {
                StartElement failureElement = xmlEventReader.nextEvent().asStartElement();
                StringBuilder log = new StringBuilder();
                while (xmlEventReader.hasNext()
                        && !isEndElement(xmlEventReader.peek(), "failure")) {
                    XMLEvent possibleText = xmlEventReader.nextEvent();
                    if (possibleText.isCharacters()) {
                        log.append(possibleText.asCharacters().getData());
                    }
                }
                testCaseReport.classifyAsFailed(
                        getAttribute(failureElement, "type", ""),
                        getAttribute(failureElement, "message", ""),
                        log.toString()
                );
            }
            else {
                xmlEventReader.nextEvent();
            }
        }
        return testCaseReport;
    }

    private boolean isStartElement(XMLEvent xmlEvent, String localName) {
        return xmlEvent.isStartElement()
                && xmlEvent.asStartElement().getName().getLocalPart().equals(localName);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean isEndElement(XMLEvent xmlEvent, String localName) {
        return xmlEvent.isEndElement()
                && xmlEvent.asEndElement().getName().getLocalPart().equals(localName);
    }


    private Duration getTime(StartElement element, Duration defaultValue) {
        double durationSeconds = getAttribute(element, "time", Double.MIN_VALUE);
        if (durationSeconds == Double.MIN_VALUE) {
            return defaultValue;
        }
        long durationMillis = Math.round(durationSeconds * 1000d);
        return Duration.of(durationMillis, ChronoUnit.MILLIS);
    }

    private Instant getTimestamp(StartElement element, Instant defaultValue) {
        String value = getAttribute(element, "timestamp", null);
        try {
            return Instant.parse(value + "Z");
        }
        catch (NullPointerException | DateTimeParseException ignored) {
            return defaultValue;
        }
    }


}
