variables:
  GRADLE_USER_HOME: $CI_PROJECT_DIR/.gradle
  GRADLE_OPTS: "-Dorg.gradle.daemon=false -Dorg.gradle.internal.launcher.welcomeMessageEnabled=false"

stages:
- test
- build
- itest
- release


# test the code with a few available Java versions

.test:
  stage: test
  script:
  - ./gradlew -q check
  only:
    - branches
    - merge_requests
  after_script:
  - ./gradlew -q codeCoverageReport testReport -x test
  - mkdir $CI_JOB_NAME
  - mv ./build/output/test-coverage ${CI_JOB_NAME}
  - mv ./build/output/test-results ${CI_JOB_NAME}
  artifacts:
    name: "$CI_COMMIT_REF_SLUG-$CI_JOB_NAME"
    when: always
    paths:
    - "${CI_JOB_NAME}"
    reports:
      junit: "*/build/test-results/test/TEST-*.xml"
    expire_in: 1 hour

test-jdk8:
  extends: .test
  image: openjdk:8-jdk-alpine

test-jdk9:
  extends: .test
  image: adoptopenjdk/openjdk9:x86_64-ubuntu-jdk-9.0.4.11

test-jdk11:
  extends: .test
  image: openjdk:11-jdk-slim

test-jdk13:
  extends: .test
  image: openjdk:13-jdk-slim


# building javadoc to verify valid documentation

test-javadoc:
  stage: test
  image: openjdk:8-jdk-alpine
  only:
  - branches
  - merge_requests
  script:
  - ./gradlew -q javadoc


# install the library using Java 8

install-jdk8:
  stage: build
  image: openjdk:8-jdk-alpine
  only:
  - branches
  - merge_requests
  dependencies: []
  script:
  - ./gradlew -q assemble


# Dry run of publication

publish-dry-jdk8:
  stage: itest
  image: openjdk:8-jdk-alpine
  dependencies: []
  variables:
    GROUP: "org.codefetti.junit.report"
    GROUP_PATH: "org/codefetti/junit/report"
    MODULE_PREFIX: "junit-report-"
  script:
  - apk add unzip
  - ./gradlew -q publishToMavenLocal
  - FILES=$(ls -R ~/.m2/repository/$GROUP_PATH)
  - >
    for module in $(ls | grep "$MODULE_PREFIX" | sed 's#/##'); do
      for type in .pom .jar -javadoc.jar -sources.jar; do
        FILE="${module}-unspecified${type}"
        echo "Checking existence of $FILE"
        echo "$FILES" | grep "$FILE"
      done
      unzip ~/.m2/repository/${GROUP_PATH}/${module}/unspecified/${module}-unspecified.jar META-INF/maven/${GROUP}/${module}/pom.xml
    done


# create a release tag

release:
  stage: release
  image: registry.gitlab.com/codefetti/git-semver/git-semver:0.1
  only:
  - master
  except:
    variables:
    - $CI_COMMIT_MESSAGE =~ /^Prepare release /
  dependencies: []
  before_script:
  - git config --global user.email "${PIPER_EMAIL}"
  - git config --global user.name "${PIPER_NAME}"
  - git remote remove origin
  - git remote add origin https://oauth2:${PIPER_ACCESS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git
  - git checkout -b "${CI_COMMIT_REF_NAME}"
  script:
  # Determine next version
  - BUMP=$(bump_yaml.sh get)
  - LAST=$(latest_tag_in_branch.sh)
  - NEXT=$(semver.sh bump -v "${LAST:-0.0.0}" -b "$BUMP")
  # Update release notes in CHANGELOG.md
  - changelog.sh release -l "$LAST" -v "$NEXT" -p "$CI_PROJECT_URL" > CHANGELOG.new.md
  - mv CHANGELOG.new.md CHANGELOG.md
  - changelog.sh extract -v "${NEXT}"
  - git commit -a --allow-empty -m "Prepare release ${NEXT}"
  - git tag -a "${NEXT}" -m "Release ${NEXT}"
  - git push origin "${CI_COMMIT_REF_NAME}" && git push origin tag $NEXT
  # add release notes to tag
  - "BODY=$(changelog.sh extract -v $NEXT | jq . -RcMs | jq '{description: .}' -cM)"
  - 'JSON="Content-Type: application/json"'
  - 'AUTH="PRIVATE-TOKEN: $PIPER_ACCESS_TOKEN"'
  - URL="https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/repository/tags/${NEXT}/release"
  - curl --header "$JSON" --header "$AUTH" --data "$BODY" --request POST "$URL"
  # switch to develop to finish the release
  - git checkout -b "${CI_DEFAULT_BRANCH}"
  - git rebase "${CI_COMMIT_REF_NAME}"
  # reset bump
  - bump_yaml.sh set -b "patch" > CHANGELOG.new.md
  - mv CHANGELOG.new.md CHANGELOG.md
  - git commit -a --allow-empty -m "Reset bump after release ${NEXT}"
  # push with tag
  - git push origin "${CI_DEFAULT_BRANCH}"


# publish from merge requests

# publish snapshot release of merge requests to GitLab packages
publishSnapshot:
  stage: release
  only:
  - merge_requests
  image: openjdk:8-jdk-alpine
  dependencies: []
  variables:
    VERSION: MR-${CI_MERGE_REQUEST_IID}-SNAPSHOT
  script:
  - ./gradlew -q clean assemble publishGitlab -PjobToken=${CI_JOB_TOKEN} -Pversion=${VERSION}


# publish from tags

# publish release to GitLab packages
publishRelease:
  stage: release
  only:
  - /^\d+.\d+.\d+$/
  except:
  - branches
  image: openjdk:8-jdk-alpine
  dependencies: []
  variables:
    VERSION: "${CI_COMMIT_TAG}"
  script:
  - ./gradlew -q clean assemble publishGitlab -PjobToken=${CI_JOB_TOKEN} -Pversion=${VERSION}
