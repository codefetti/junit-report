package org.codefetti.junit.report.model;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the test result summary of a test suite.
 */
public class TestSuiteReport {

    private String name;

    private long tests;

    private long skipped;

    private long failures;

    private long errors;

    private Instant timeOfExecution;

    private Duration durationOfExecution;

    private List<TestCaseReport> testCases = new ArrayList<>();

    /**
     * Creates a new {@link TestSuiteReport} with basic information. {@link TestCaseReport}s can
     * be added after creation using {@link #addTestCase(TestCaseReport)}
     * @param name The name of the test suite. In Java tests this name refers to the fully
     *             qualified class name, e.g. {@code my.tool.package.FooTest}. This property
     *             refers to the {@code name} attribute of the {@code testsuite} element.
     * @param tests The total number of tests in this suite. This property refers to the
     *              {@code tests} attribute of the {@code testsuite} element.
     * @param skipped The number of skipped or ignored tests in this suite. This property refers
     *                to the {@code skipped} attribute of the {@code testsuite} element.
     * @param failures The number of tests with failures in this test suite. These tests have
     *                 failed assertions. This property refers to the {@code failures} attribute
     *                 of the {@code testsuite} element.
     * @param errors The number of tests that failed to execute in this test suite. This property
     *               refers to the {@code errors} attribute of the {@code testsuite} element.
     * @param timeOfExecution When this suite has been executed. This property refers to the
     *                        {@code timestamp} attribute of {@code testsuite} element.
     * @param durationOfExecution The time needed to execute this test suite. This property
     *                            refers to the {@code time} attribute of the {@code testsuite}
     *                            element which is expected to be in seconds with fractional
     *                            milliseconds.
     */
    public TestSuiteReport(
            String name,
            long tests,
            long skipped,
            long failures,
            long errors,
            Instant timeOfExecution, Duration durationOfExecution) {
        this.name = name;
        this.tests = tests;
        this.skipped = skipped;
        this.failures = failures;
        this.errors = errors;
        this.timeOfExecution = timeOfExecution;
        this.durationOfExecution = durationOfExecution;
    }

    /**
     * Adds a new {@link TestCaseReport} to the end of list of test cases.
     * @param testCaseReport The results of an individual test case in this suite. This property
     *                      refers to one {@code testcase} element in the {@code testsuite}
     *                      element.
     */
    public void addTestCase(TestCaseReport testCaseReport) {
        testCases.add(testCaseReport);
    }

    /**
     * @return The name of the test suite. In Java tests this name refers to the fully qualified
     * class name, e.g. {@code my.tool.package.FooTest}. This property refers to the {@code name}
     * attribute of the {@code testsuite} element.
     */
    public String getName() {
        return name;
    }

    /**
     * @return The total number of tests in this suite. This property refers to the {@code tests}
     * attribute of the {@code testsuite} element.
     */
    public long getTests() {
        return tests;
    }

    /**
     * @return The number of skipped or ignored tests in this suite. This property refers to the
     * {@code skipped} attribute of the {@code testsuite} element.
     */
    public long getSkipped() {
        return skipped;
    }

    /**
     * @return The number of tests with failures in this test suite. These tests have failed
     * assertions. This property refers to the {@code failures} attribute of the
     * {@code testsuite} element. Note that often the {@linkplain #getErrors() errors} are
     * included in the failures and not noted separately.
     */
    public long getFailures() {
        return failures;
    }

    /**
     * @return The number of tests that failed to execute in this test suite. This property
     * refers to the {@code errors} attribute of the {@code testsuite} element. Note that the
     * errors are often mangled into the {@linkplain #getFailures() failures} and not noted
     * separately.
     */
    public long getErrors() {
        return errors;
    }

    /**
     * @return When this suite has been executed. This property refers to the {@code timestamp}
     * attribute of {@code testsuite} element.
     */
    public Instant getTimeOfExecution() {
        return timeOfExecution;
    }

    /**
     * @return The time needed to execute this test suite. This property refers to the
     * {@code time} attribute of the {@code testsuite} element which is expected to be in seconds
     * with fractional milliseconds.
     */
    public Duration getDurationOfExecution() {
        return durationOfExecution;
    }

    /**
     * @return The results of the individual test cases in this suite. This property refers to
     * the {@code testcase} elements in the {@code testsuite} element.
     */
    public List<TestCaseReport> getTestCases() {
        return testCases;
    }
}
