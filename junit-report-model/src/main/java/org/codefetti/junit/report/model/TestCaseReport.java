package org.codefetti.junit.report.model;

import java.time.Duration;

/**
 * Represents the test results of a single test case. This class refers to the {@code testcase}
 * element.
 */
public class TestCaseReport {

    public enum Result {PASSED, FAILED, SKIPPED}

    private String className;

    private String name;

    private Result result;

    private Duration durationOfExecution;

    private String failureType;

    private String failureMessage;

    /**
     * The log of the failure usually refers to the stacktrace of the thrown exception that caused
     * the failure. This property refers to the content of the {@code failure} element. This
     * property is {@code null} if {@link #result} is not {@link Result#FAILED}.
     */
    private String failureLog;

    /**
     * Creates a new {@link Result#PASSED} {@link TestCaseReport}. The result of the report can
     * be changed on the created instance using {@link #classifyAsSkipped()} or
     * {@link #classifyAsFailed(String, String, String)}.
     *
     * @param className The fully qualified name of the class that executed the test. It refers to
     *                  the {@code classname} attribute of the {@code testcase} element.
     * @param name The name of the test case which is usually the name of the test method but may
     *             also  be an artificial name based on a naming template for test cases that run
     *             multiple times with different input. It refers to the {@code name} attribute
     *             of the {@code testcase} element.
     * @param durationOfExecution The time needed to execute this test case. It refers to the
     *                            {@code time} attribute of the {@code testcase} element which is
     *                            expected to be in seconds with fractional milliseconds.
     */
    public TestCaseReport(String className, String name, Duration durationOfExecution) {
        this.className = className;
        this.name = name;
        this.durationOfExecution = durationOfExecution;
        this.result = Result.PASSED;
    }

    /**
     * Classifies this {@link TestCaseReport} as {@link Result#SKIPPED} and resets the failure
     * information.
     */
    public void classifyAsSkipped() {
        failureType = null;
        failureMessage = null;
        failureLog = null;
        result = Result.SKIPPED;
    }

    /**
     * Classifies this {@link TestCaseReport} as {@link Result#SKIPPED} and resets the failure
     * information.
     * @param failureType The type of the failure which usually refers to a fully qualified
     *                    exception class name. It refers to the {@code type} attribute of the
     *                    {@code failure} element.
     * @param failureMessage The message of the failure which usually refers to the message of
     *                       the thrown exception that caused the failure. This property refers
     *                       to the {@code message} attribute of the {@code failure} element.
     * @param failureLog The log of the failure which usually refers to the stacktrace of the
     *                   thrown exception that caused the failure. This property refers to the
     *                   content of the {@code failure} element.
     */
    public void classifyAsFailed(String failureType, String failureMessage, String failureLog) {
        this.failureType = failureType;
        this.failureMessage = failureMessage;
        this.failureLog = failureLog;
        result = Result.FAILED;
    }

    /**
     * @return The fully qualified name of the class that executed the test. It refers to the
     * {@code classname} attribute of the {@code testcase} element.
     */
    public String getClassName() {
        return className;
    }

    /**
     * @return The name of the test case which is usually the name of the test method but may also
     * be an artificial name based on a naming template for test cases that run multiple times with
     * different input. It refers to the {@code name} attribute of the {@code testcase} element.
     */
    public String getName() {
        return name;
    }

    /**
     * @return If the testcase passed, failed or has been skipped. At this level, JUnit XML reports
     * do not provide a difference of failure or error. The test case is considered as
     * {@code passed} if there is no {@code failure} element and no {@code skipped} element in the
     * {@code testcase} element.
     */
    public Result getResult() {
        return result;
    }

    /**
     * @return The time needed to execute this test case. It refers to the {@code time} attribute
     * of the {@code testcase} element which is expected to be in seconds with fractional
     * milliseconds.
     */
    public Duration getDurationOfExecution() {
        return durationOfExecution;
    }

    /**
     * @return The type of the failure which usually refers to a fully qualified exception class
     * name. It refers to the {@code type} attribute of the {@code failure} element. Returns
     * {@code null} if {@link #getResult()} does not return {@link Result#FAILED}.
     */
    public String getFailureType() {
        return failureType;
    }

    /**
     * @return The message of the failure which usually refers to the message of the thrown
     * exception that caused the failure. This property refers to the {@code message} attribute
     * of the {@code failure} element. Returns {@code null} if {@link #getResult()} does not return
     * {@link Result#FAILED}.
     */
    public String getFailureMessage() {
        return failureMessage;
    }

    /**
     * @return The log of the failure which usually refers to the stacktrace of the thrown
     * exception that caused the failure. This property refers to the content of the
     * {@code failure} element. Returns {@code null} if {@link #getResult()} does not return
     * {@link Result#FAILED}.
     */
    public String getFailureLog() {
        return failureLog;
    }
}
