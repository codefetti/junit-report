---
# This Yaml header defines the version bump as major, minor or patch.
bump: patch
---
# Changelog

All notable changes to Codefetti JUnit Report will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## Unreleased

No notable changes yet.

